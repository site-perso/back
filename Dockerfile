FROM python:3.6-slim

RUN apt update && apt install -y gcc libpq-dev

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

EXPOSE 8000

CMD ["python", "/usr/src/app/manage.py", "runserver", "0.0.0.0:8000"]